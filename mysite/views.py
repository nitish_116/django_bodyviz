from django.shortcuts import render
from django.http import HttpResponse,HttpResponseRedirect
# from django.template import Context, loader
from django.views.decorators.csrf import csrf_exempt
from django.template.context_processors import csrf
# from django.conf import settings
# from django.template import RequestContext
# import json
# import os
# import cPickle
# from collections import OrderedDict
# from django.shortcuts import render_to_response,redirect
# from django.contrib.auth.decorators import login_required
# from django.contrib.auth import authenticate, login, logout
# from django.contrib import auth

# Create your views here.

def index(request):
    C = {}
    C.update(csrf(request))
    return render(request,'index.html',C)
    # return HttpResponse("nitish")

def func_file_creation(request):
    # print(request.GET.keys())
    garment_data = request.GET['vertices_data']
    fileid = request.GET['filename']
    print(fileid)
    import os,sys,inspect
    BASE_DIR = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe()))) + "/"
    vertex_folder = 'vertex_folder'
    vertex_folder_path = BASE_DIR + vertex_folder + "/"
    
    # os.system("rm -rf "+vertex_folder_path)
    # os.system("mkdir "+vertex_folder_path)
    current_vertex_file_path = vertex_folder_path + str(fileid) + '.txt'
    print(fileid,current_vertex_file_path)
    f = open(current_vertex_file_path,'w')
    f.write(str(garment_data))
    f.close()
    return HttpResponse("file saved")
