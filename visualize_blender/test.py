def draw_pattern(name,coord_list,face_list,reverse=False):

      me = bpy.data.meshes.new(name)
      ob = bpy.data.objects.new(name, me)
      scn = bpy.context.scene
      scn.objects.link(ob)
      scn.objects.active = ob
      reversed_face_list = []
      if reverse:
        for face in face_list:
            reversed_face_list.append(face.reverse())
      
      me.from_pydata(coord_list,[],face_list)
import sys,os,inspect
BASE_DIR = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe()))) + "/"
sys.path.append(BASE_DIR)

from data import name, mean_vertices,mean_faces
from waist_circumference_pref_plus_5mm import waist_circumference_pref_plus_5mm_vertices
from chest_circumference_plus_5mm_faces import chest_circumference_plus_5mm_vertices

import bpy
for ob in bpy.data.objects:
      ob.select = True
bpy.ops.object.delete(use_global=False)

mod_verts = []
mod_faces = []
for cur_vert in mean_vertices:
      mod_verts.append(tuple(cur_vert))

for cur_face in mean_faces:
      mod_faces.append(tuple(cur_face))


waist_verts = []
for cur_vert in waist_circumference_pref_plus_5mm_vertices:
      waist_verts.append(tuple(cur_vert))

chest_verts = []
for cur_vert in chest_circumference_plus_5mm_vertices:
      chest_verts.append(tuple(cur_vert))

import skel2
from imp import reload
reload(skel2)
from skel2 import test
all_vertices = test
skel_verts = []
for vert_ind in range(len(mod_verts)):
      s0 = str(vert_ind*3)
      s1 = str(vert_ind*3 + 1)
      s2 = str(vert_ind*3 + 2) 
      tmp_vert = (all_vertices[s0],all_vertices[s1], all_vertices[s2])
      skel_verts.append(tmp_vert)

# draw_pattern('test',[(0,0,0),(1,0,0),(0,1,0)],[(0,1,2)])
# draw_pattern('mean',mod_verts,mod_faces)
# draw_pattern('waist',waist_verts,mod_faces)
# draw_pattern('chest',chest_verts,mod_faces)
draw_pattern('skel',skel_verts,mod_faces)
